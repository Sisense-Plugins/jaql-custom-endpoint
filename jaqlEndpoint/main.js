//	Initialize the copyWidget object
prism.run([ function() {	

	//////////////////////////
	//	Utility Functions	//
	//////////////////////////

    function elasticubeSupported(dashboard) {

        //  Are all Elasticubes following this rule?
        if (config.elasticubes.all) {

            //  Yes, OK to run
            return true;

        } else {

            //  Get the name of the Elasticube for this dashboard
            var ec = typeof dashboard.datasource == "object" ? dashboard.datasource.title : dashboard.datasource;

            //  Look for the Elasticube title in the list of supported Elasticubes (config.js)
            var isSupported = (config.elasticubes.members.indexOf(ec) >= 0);

            //  Return the result
            return isSupported;
        }
    }


    //////////////////////////////////
    //  Main Functions              //
    //////////////////////////////////

    //  Function to turn on/off the value labels
    function customizeQuery(widget,args){

        //  Modify the JAQL to include an 'endpoint' attribute,
        //  which will change where the query gets sent.
        //  Note that this will result in an OPTIONS request first,
        //  and then the POST request
        args.query.endpoint = config.endpoint;  
    }


    //////////////////////////////////
    //  Initialization Functions    //
    //////////////////////////////////

	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (event, args) {
        
        //  Make sure we should run this logic
        var shouldRun = elasticubeSupported(args.dashboard);
        if (shouldRun) {

            //  Add the event handler for this dashboard's widget queries
            args.dashboard.on("widgetbeforequery", customizeQuery);
        }
	});

}]);